1
00:00:08,495 --> 00:00:09,832
All eyes on me now

2
00:00:09,832 --> 00:00:11,152
If you still doubt mine

3
00:00:11,152 --> 00:00:12,496
It’s too pointless

4
00:00:12,496 --> 00:00:13,864
We’re still young and wild

5
00:00:13,864 --> 00:00:16,175
We gonna find new world to be mine

6
00:00:16,175 --> 00:00:17,561
The map calls for this

7
00:00:17,561 --> 00:00:20,021
Island of dream and fear of all hearts

8
00:00:20,021 --> 00:00:22,253
Dream starts in the end, wings spread,

9
00:00:22,253 --> 00:00:24,302
Forever child like Peter

10
00:00:24,302 --> 00:00:25,678
Bursting

11
00:00:25,678 --> 00:00:28,014
Hearts pump it up

12
00:00:28,014 --> 00:00:29,430
Relaxing

13
00:00:29,430 --> 00:00:31,318
Fists hold it on

14
00:00:31,318 --> 00:00:35,367
Facing the door of truth opening,

15
00:00:35,367 --> 00:00:38,911
We on fire to smash

16
00:00:38,911 --> 00:00:42,312
Are you going to stop here?

17
00:00:42,312 --> 00:00:46,552
What you longed forin front of your eyes oh

18
00:00:46,552 --> 00:00:49,809
I know I can’t come back

19
00:00:49,809 --> 00:00:53,529
Oh, we must going on

20
00:00:53,529 --> 00:00:57,194
Let’s go

21
00:00:57,194 --> 00:00:59,050
Let’s go

22
00:00:59,050 --> 00:01:00,866
To the start where the end awaits

23
00:01:00,866 --> 00:01:04,650
On my my way we set ourselves to run

24
00:01:04,650 --> 00:01:08,323
On my my way on the count of three

25
00:01:08,323 --> 00:01:11,988
On my my way make up our own ways

26
00:01:11,988 --> 00:01:13,843
Let’s hurry and get going

27
00:01:13,843 --> 00:01:15,946
To the start where the end awaits

28
00:01:19,466 --> 00:01:21,542
Ready now for next journey

29
00:01:21,542 --> 00:01:23,903
Hole your breath I run and get money

30
00:01:23,903 --> 00:01:25,335
I got a win I shout out louder

31
00:01:25,335 --> 00:01:27,183
Tambourine beating louder than the Acre

32
00:01:27,183 --> 00:01:28,295
Higher than Jack Sparrow

33
00:01:28,295 --> 00:01:30,304
Go and I go without a blink

34
00:01:30,304 --> 00:01:32,248
I beat BANG BANG at once my breath

35
00:01:32,248 --> 00:01:35,032
Is like down with us get it going

36
00:01:35,032 --> 00:01:36,785
I don’t care

37
00:01:36,785 --> 00:01:38,657
If it blows. I am,

38
00:01:38,657 --> 00:01:39,633
Here are,

39
00:01:39,633 --> 00:01:41,434
The start of the end

40
00:01:41,434 --> 00:01:45,418
Facing the door of truth opening,

41
00:01:45,418 --> 00:01:49,354
We on fire to smash

42
00:01:49,354 --> 00:01:52,546
Are you going to stop here?

43
00:01:52,546 --> 00:01:56,739
What you longed for in front of your eyes oh

44
00:01:56,739 --> 00:02:00,043
I know I can’t come back

45
00:02:00,043 --> 00:02:03,708
Oh, we must going on

46
00:02:03,708 --> 00:02:07,332
Let’s go

47
00:02:07,332 --> 00:02:09,172
Let’s go

48
00:02:09,172 --> 00:02:11,053
To the start where the end awaits

49
00:02:11,053 --> 00:02:14,741
On my my way we set ourselves to run

50
00:02:14,741 --> 00:02:18,398
On my my way on the count of three

51
00:02:18,398 --> 00:02:22,118
On my my way make up our own ways

52
00:02:22,118 --> 00:02:23,990
Let’s hurry and get going

53
00:02:23,990 --> 00:02:26,189
To the start where the end awaits

54
00:02:29,949 --> 00:02:36,479
Breaking the wall, oh oh

55
00:02:36,479 --> 00:02:38,318
Someone

56
00:02:38,318 --> 00:02:40,303
Has to go through, anyway

57
00:02:40,303 --> 00:02:44,551
It’s not easy

58
00:02:44,551 --> 00:02:46,246
But that’s why we like it

59
00:02:48,908 --> 00:02:49,856
Run run

60
00:02:52,517 --> 00:02:53,656
Run run

61
00:02:56,119 --> 00:02:57,224
Run run

62
00:02:57,224 --> 00:02:59,072
Let’s hurry get going

63
00:02:59,072 --> 00:03:00,888
To the start where the end awaits

64
00:03:00,888 --> 00:03:04,593
On my my way we set ourselves to run

65
00:03:04,593 --> 00:03:08,289
On my my way on the count of three

66
00:03:08,289 --> 00:03:11,962
On my my way make up our own ways

67
00:03:11,962 --> 00:03:13,954
Let’s hurry and get going

68
00:03:13,954 --> 00:03:15,738
To the start where the end awaits

69
00:03:15,738 --> 00:03:17,954
Let’s go

